import Graph from "./pages/Graph";
import "@react-sigma/core/lib/react-sigma.min.css";

function App() {
    return (
        <div className="app">
            <Graph/>
        </div>
    );
}

export default App;
