import {Button, ColorPicker, Divider, Form, Input, Modal, Select, Typography} from 'antd';

const {Title} = Typography;

export const GraphModal = ({openModal, onCancelModal, onFinishForm, nodes, isGraphs}) => {

    const selectNodesOptions = nodes.map((item) => {
        return {
            value: item.key,
            label: item.label,
        }
    });

    const handleFinishForm = (values) => {
        onFinishForm(values);
    };

    return (
        <Modal
            centered
            destroyOnClose
            open={openModal}
            title={(
                <>
                    <Title level={4}>Add Node</Title>
                    <Divider/>
                </>
            )}
            onCancel={onCancelModal}
            footer={[
                <Divider/>,
                <Button key="cancel" onClick={onCancelModal}>
                    Cancel
                </Button>,
                <Button form="add_node" htmlType="submit" key="add" type="primary">
                    Add
                </Button>,
            ]}
        >
            <Form onFinish={handleFinishForm} id="add_node" name="add_node" layout="vertical" autoComplete="off">
                <Form.Item
                    name="label"
                    label="Name :"
                    rules={[{required: true, message: "node name is required!"}]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    name="info"
                    label="Information :"
                    rules={[{required: true, message: "Information is required!"}]}
                >
                    <Input/>
                </Form.Item>
                {isGraphs && (
                    <Form.Item
                        name="relations"
                        label="Relations :"
                        rules={[{required: true, message: "relation is required!"}]}
                    >
                        <Select mode="multiple" options={selectNodesOptions}/>
                    </Form.Item>
                )}
                <Form.Item
                    name="color"
                    label="Color :"
                    rules={[{required: true, message: 'node color is required!'}]}
                    initialValue={null}
                >
                    <ColorPicker format="hex" style={{width: "100%"}} disabledAlpha showText/>
                </Form.Item>
            </Form>
        </Modal>
    );
};
