import {useEffect} from "react";
import {useLoadGraph} from "@react-sigma/core";
import {useLayoutCircular} from "@react-sigma/layout-circular";
import {MultiDirectedGraph} from "graphology";

export const GraphNodes = ({graphs, relations}) => {
    const loadGraph = useLoadGraph();
    const { assign } = useLayoutCircular();

    useEffect(() => {
        const graph = new MultiDirectedGraph();

        graphs.forEach(item => {
            graph.addNode(item.key, {x: item.x, y: item.y, label: item.label, size: 10, color: item.color});
        });

        relations.forEach(item => {
            graph.addEdgeWithKey(item.key, item.from, item.to, {label: item.label});
        });

        loadGraph(graph);
        assign();

    }, [assign, graphs, loadGraph, relations]);

    return null;
}