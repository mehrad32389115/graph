import {useEffect} from "react";
import {useRegisterEvents} from "@react-sigma/core";

export const GraphEvents = ({onClickNode}) => {
    const registerEvents = useRegisterEvents();

    useEffect(() => {
        registerEvents({
            clickNode: (event) => onClickNode(event.node),
        });
    }, [ onClickNode, registerEvents]);

    return null;
};