import {SigmaContainer} from "@react-sigma/core";
import {GraphEvents} from "./features/GraphEvents";
import {GraphNodes} from "./features/GraphNodes";

export const CustomGraph = ({graphs, relations, onClickNode}) => {
    return (
        <SigmaContainer style={{height: "500px"}}>
            <GraphNodes graphs={graphs} relations={relations}/>
            <GraphEvents onClickNode={onClickNode}/>
        </SigmaContainer>
    );
};
