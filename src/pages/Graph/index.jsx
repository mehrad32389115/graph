import {useState} from "react";
import {Button, Card, Col, notification, Row} from "antd";
import {PlusOutlined} from "@ant-design/icons";
import {CustomGraph} from "../../components/CustomGraph";
import {GraphModal} from "../../components/GraphModal";
import style from "./graph.module.css";

function DisplayGraph() {
    const [graphs, setGraphs] = useState([]);
    const [relations, setRelations] = useState([])
    const [openGraphModal, setOpenGraphModal] = useState(false);

    const onOpenModal = () => {
        setOpenGraphModal(true);
    };

    const handleCancelModal = () => {
        setOpenGraphModal(false);
    };

    const onFinishForm = (values) => {
        setGraphs(prevState => [...prevState,
            {
                key: graphs.length !== 0 ? prevState[prevState.length - 1].key + 1 : 0,
                x: graphs.length !== 0 ? prevState[prevState.length - 1].x + 1 : 0,
                y: graphs.length !== 0 ? prevState[prevState.length - 1].y + 1 : 0,
                label: values.label,
                color: values.color.toHexString(),
                info: values.info
            },
        ]);

        if (values.relations) {
            values.relations.forEach(item => {
                setRelations(prevState => [...prevState,
                    {
                        key: `rel${prevState.length}`,
                        label: `REL_${prevState.length}`,
                        from: graphs[graphs.length - 1].key + 1,
                        to: item,
                    },
                ]);
            });
        }
        setOpenGraphModal(false);
    };

    const handleClickNode = (nodeKey) => {
        const findNode = graphs.find(item => item.key === parseInt(nodeKey));
        notification.open({
            message: 'Node Information',
            description: findNode.info,
        });
    };

    return (
        <div className={style.wrapper}>
            <Row gutter={[0, 16]}>
                <Col span={24}>
                    <Button icon={<PlusOutlined/>} type="primary" onClick={onOpenModal}>Add Node</Button>
                </Col>
                <Col span={24}>
                    <Card>
                        <CustomGraph graphs={graphs} relations={relations} onClickNode={handleClickNode}/>
                    </Card>
                </Col>
            </Row>
            <GraphModal
                openModal={openGraphModal}
                onCancelModal={handleCancelModal}
                onFinishForm={onFinishForm}
                nodes={graphs}
                isGraphs={graphs.length !== 0}
            />
        </div>
    );
}

export default DisplayGraph